package com.filmoteka.service;

import com.filmoteka.repository.DirectorRepo;
import com.filmoteka.repository.FilmRepo;
import com.filmoteka.dto.FilmDTO;
import com.filmoteka.mapper.FilmMapper;
import com.filmoteka.model.Director;
import com.filmoteka.model.Film;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import static org.springframework.data.relational.core.sql.StatementBuilder.update;

@Service
public class FilmService
        extends GenericService<Film, FilmDTO> {
    private final DirectorRepo directorRepo;

    protected FilmService(FilmRepo repository,
                          FilmMapper mapper,
                          DirectorRepo directorRepo) {
        super(repository, mapper);
        this.directorRepo = directorRepo;
    }

    public FilmDTO addDirector(final Long filmID,
                             final Long directorID) throws NotFoundException {
        FilmDTO film = getOne_returneDTO(filmID);
        Director director = directorRepo.findById(directorID).orElseThrow(() -> new NotFoundException("Автор не найден"));
        film.getDirectorIDList().add(director.getId());
        updateEntity_returnDTO(film);
        return film;
    }
}
