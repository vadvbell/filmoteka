package com.filmoteka.service;

import com.filmoteka.repository.UserRepo;
import com.filmoteka.dto.RoleDTO;
import com.filmoteka.dto.UserDTO;
import com.filmoteka.mapper.UserMapper;
import com.filmoteka.model.User;
import org.springframework.stereotype.Service;

@Service
public class UserService extends GenericService<User, UserDTO> {

    public UserService(UserRepo repo,
                       UserMapper mapper) {
        super(repo, mapper);
    }
    @Override
    public UserDTO createNewEntity_returnDTO(UserDTO newObject){
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        newObject.setRole(roleDTO);
        return mapper.toDTO(repo.save(mapper.toEntity(newObject)));
    }
}
