package com.filmoteka.service;

import com.filmoteka.repository.OrderRepo;
import com.filmoteka.dto.OrderDTO;
import com.filmoteka.mapper.OrderMapper;
import com.filmoteka.model.Order;
import org.springframework.stereotype.Service;

@Service
public class OrderService extends GenericService<Order, OrderDTO> {
    protected OrderService(OrderRepo repo,
                           OrderMapper mapper) {
        super(repo, mapper);
    }
}
