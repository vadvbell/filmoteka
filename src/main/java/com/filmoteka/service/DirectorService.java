package com.filmoteka.service;

import com.filmoteka.mapper.GenericMapper;
import com.filmoteka.repository.FilmRepo;
import com.filmoteka.repository.GenericRepo;
import com.filmoteka.dto.DirectorDTO;
import com.filmoteka.model.Director;
import com.filmoteka.model.Film;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

@Service
public class DirectorService extends GenericService<Director, DirectorDTO>{
    private final FilmRepo filmRepo;
    public DirectorService(GenericRepo<Director> repo,
                           GenericMapper<Director, DirectorDTO> mapper,
                           FilmRepo filmRepo) {
        super(repo, mapper);
        this.filmRepo = filmRepo;
    }
    public DirectorDTO addFilm(Long filmID, Long directorID) throws NotFoundException {
        Film film = filmRepo.findById(filmID).orElseThrow(() -> new NotFoundException("Нет фильма с id -> " + filmID));
        DirectorDTO directorDTO = getOne_returneDTO(directorID);
        directorDTO.getFilmIDList().add(film.getId());
        updateEntity_returnDTO(directorDTO);
        return directorDTO;
    }
}
