package com.filmoteka.service;

import com.filmoteka.mapper.GenericMapper;
import com.filmoteka.repository.GenericRepo;
import com.filmoteka.dto.GenericDTO;
import com.filmoteka.model.RootModel;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public abstract class GenericService<MOD extends RootModel, DTO extends GenericDTO> {
    protected final GenericRepo<MOD> repo;
    protected final GenericMapper<MOD, DTO> mapper;

    public GenericService(GenericRepo<MOD> repo,
                          GenericMapper<MOD, DTO> mapper) {
        this.repo = repo;
        this.mapper = mapper;
    }
    public List<DTO> listAll_returnListDTO(){
        return mapper.toDTOList(repo.findAll());
    }
    public DTO getOne_returneDTO(final Long id) throws NotFoundException {
        return mapper.toDTO(repo.findById(id).orElseThrow(() -> new NotFoundException("Нет такого ID -> " + id)));
    }
    public DTO createNewEntity_returnDTO(DTO newObject){
        return mapper.toDTO(repo.save(mapper.toEntity(newObject)));
    }
    public DTO updateEntity_returnDTO(DTO updateObject){
        return mapper.toDTO(repo.save(mapper.toEntity(updateObject)));
    }
    public void deleteEntityByID(final Long id){
        repo.deleteById(id);
    }
}
