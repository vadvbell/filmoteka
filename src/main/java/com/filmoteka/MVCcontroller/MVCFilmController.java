package com.filmoteka.MVCcontroller;

import com.filmoteka.dto.FilmDTO;
import com.filmoteka.service.FilmService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/films")
public class MVCFilmController {
    private FilmService filmService;
    public MVCFilmController(FilmService filmService) {
        this.filmService = filmService;
    }

    @GetMapping("")
    public String getAllFilms(Model model){
        List<FilmDTO> filmDTOList = filmService.listAll_returnListDTO();
        model.addAttribute("films", filmDTOList);
        return "films/viewAllFilms";
    }

    @GetMapping("/add")
    public String addFilmMVC(){
        return "films/addFilm";
    }
    @PostMapping("/add")
    public String addFilmMVC(@ModelAttribute("filmForm") FilmDTO newFilm){
        filmService.createNewEntity_returnDTO(newFilm);
        return "redirect:/films";
    }
}
