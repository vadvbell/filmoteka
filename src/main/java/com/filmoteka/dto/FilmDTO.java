package com.filmoteka.dto;

import com.filmoteka.model.Genre;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
public class FilmDTO extends GenericDTO {

    private String filmTitle;
    private LocalDate filmPremierYear;
    private String filmCountry;
    private Genre filmGenre;
    private List<Long> directorIDList;
}
