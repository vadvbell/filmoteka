package com.filmoteka.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
public class DirectorDTO extends GenericDTO{

    private String directorFIO;
    private String position;
    private List<Long> filmIDList;
}
