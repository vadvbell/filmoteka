package com.filmoteka.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO extends GenericDTO{
    private LocalDateTime rentDate;
    private Integer rentPeriod;
    private boolean purcashe;
    private Long userID;
    private Long filmID;
}
