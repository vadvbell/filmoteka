package com.filmoteka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;


@SpringBootApplication
public class SpringProjectFilms implements CommandLineRunner {
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	public static void main(String[] args) {
		SpringApplication.run(SpringProjectFilms.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
				System.out.println("111111111111111111");
	}
}

