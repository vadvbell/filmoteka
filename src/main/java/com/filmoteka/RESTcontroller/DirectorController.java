package com.filmoteka.RESTcontroller;

import com.filmoteka.dto.DirectorDTO;
import com.filmoteka.model.Director;
import com.filmoteka.service.DirectorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/directors")
@Tag(name = "Режиссёры", description = "Контроллер для работы с режиссёрами")
public class DirectorController extends GenericController<Director, DirectorDTO> {
    protected DirectorController(DirectorService directorService) {
        super(directorService);
    }

    @Operation(description = "Добавить Фильм к Режиссёру")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDTO> addFilm(@RequestParam(value = "director_id") Long directorId,
                                             @RequestParam(value = "film_id") Long filmId) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(((DirectorService) service).addFilm(filmId, directorId));
    }
}
