package com.filmoteka.RESTcontroller;

import com.filmoteka.dto.FilmDTO;
import com.filmoteka.model.Film;
import com.filmoteka.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/films")
@Tag(name = "Фильмы", description = "Контроллер для работы с фильмами")
public class FilmController extends GenericController<Film, FilmDTO> {
    protected FilmController(FilmService filmService) {
        super(filmService);
    }

    @Operation(description = "Добавить Режиссёра к Фильму")
    @RequestMapping(value = "/addDirector", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDTO> addFilm(@RequestParam(value = "director_id") Long directorId,
                                         @RequestParam(value = "film_id") Long filmId) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK).body(((FilmService) service).addDirector(filmId, directorId));
    }

}
