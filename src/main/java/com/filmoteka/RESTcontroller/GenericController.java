package com.filmoteka.RESTcontroller;

import com.filmoteka.dto.GenericDTO;
import com.filmoteka.model.RootModel;
import com.filmoteka.service.GenericService;
import io.swagger.v3.oas.annotations.Operation;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@Slf4j
public abstract class GenericController<MOD extends RootModel, DTO extends GenericDTO> {

    protected GenericService<MOD, DTO> service;
    protected GenericController(GenericService<MOD, DTO> service) {
        this.service = service;
    }


    @Operation(description = "Получение записи по ID", method = "getOneById")
    @RequestMapping(value = "/getOneById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTO> getOneById(@RequestParam(value = "id") Long id) throws NotFoundException {
//        log.info(genericRepo.findById(id).toString());
//        log.info(logRespEn.toString());
        return ResponseEntity.status(HttpStatus.OK)
//                .body(genericRepo.findById(id).orElseThrow(() -> new NotFoundException("Нет такого id = " + id)));
                .body(service.getOne_returneDTO(id));
    }
    @Operation(description = "Получение всех-всех записей", method = "getAll")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DTO>> getAll(){
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.listAll_returnListDTO());
    }
    @Operation(description = "Создание новой записи", method = "addRecord")
    @RequestMapping(value = "/addRecord", method = RequestMethod.POST,
                    produces = MediaType.APPLICATION_JSON_VALUE,
                    consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTO> addRecord(@RequestBody DTO newEntity){
// Это дела серверные
//        newEntity.setCreateWhen
//        newEntity.setCreatedBy
//        log.info(newEntity.toString());
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(service.createNewEntity_returnDTO(newEntity));
    }
    @Operation(description = "Обновить существующую запись", method = "updateRecord")
    @RequestMapping(value = "/updateRecord", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DTO> updateRecord(@RequestBody DTO updateEntity,
                                            @RequestParam(value = "id") Long id){
        updateEntity.setId(id);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(service.updateEntity_returnDTO(updateEntity));
    }
    @Operation(description = "Удалить запись", method = "deleteRecord")
    @RequestMapping(value = "/deleteRecord", method = RequestMethod.DELETE)
    public void deleteRecord(@RequestParam(value = "id") Long id){
        service.deleteEntityByID(id);
    }

}
