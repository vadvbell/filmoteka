package com.filmoteka.RESTcontroller;


import com.filmoteka.dto.OrderDTO;
import com.filmoteka.model.Order;
import com.filmoteka.service.OrderService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
@Tag(name = "Аренда фильмов", description = "Контроллер для работы с Арендой Фильмов")
public class OrdersController extends GenericController<Order, OrderDTO> {
    protected OrdersController(OrderService service) {
        super(service);
    }
}
