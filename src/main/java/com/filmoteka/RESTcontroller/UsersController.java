package com.filmoteka.RESTcontroller;

import com.filmoteka.dto.UserDTO;
import com.filmoteka.model.User;
import com.filmoteka.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@Tag(name = "Пользователи", description = "Контроллер для работы с Пользователями")
public class UsersController extends GenericController<User, UserDTO> {
    protected UsersController(UserService service) {
        super(service);
    }
}
