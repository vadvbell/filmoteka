package com.filmoteka.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
//@ToString
@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(name = "uniqueLogin", columnNames = "login"),
        @UniqueConstraint(name = "uniqueEmail", columnNames = "email"),
        @UniqueConstraint(name = "uniquePhone", columnNames = "phone")
        })
@SequenceGenerator(name="default_generator", sequenceName = "users_seq", allocationSize = 1)
public class User extends RootModel {
    @Column(name = "login", nullable = false)
    private String login;
    @Column(name = "password",nullable = false)
    private String password;
    @Column(name = "first_name", nullable = false)
    private String firstName;
    @Column(name = "last_name", nullable = false)
    private String lastName;
    @Column(name = "middle_name")
    private String middleName;
    @Column(name = "birth_date", nullable = false)
    private LocalDate birthDate;
    @Column(name = "phone", nullable = false)
    @Pattern(regexp = "^\\d{11}$", message = "Телефонный номер должен состоять из 11 цифр")
    private String phoneNumber;
    @Column(name = "adress")
    private String adress;
    @Column(name = "email", nullable = false)
    private String email;
    @Column(name = "created_when", nullable = false)
    private LocalDateTime createdWhen;
    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false, foreignKey = @ForeignKey(name = "FKEY_USERS_ROLES"))
    private Role roleId;

    @OneToMany(mappedBy = "users", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Order> filmsRentList;
}
