package com.filmoteka.model;

public enum Genre {
    COMEDY("Комедия"),
    MYSTIQUE("Мистика"),
    DRAMA("Драма"),
    MELODRAMA("Мелодрама");

    private final String genreTextDisplay;

    Genre(String text) {
        this.genreTextDisplay = text;
    }

    public String getGenreTextDisplay() {
        return this.genreTextDisplay;
    }
}
