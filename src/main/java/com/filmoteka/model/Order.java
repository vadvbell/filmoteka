package com.filmoteka.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
//@ToString
@Entity
@Table(name = "orders")
@SequenceGenerator(name="default_generator", sequenceName = "orders_seq", allocationSize = 1)
public class Order extends RootModel {
    @Column(name ="rent_date", nullable = false)
    private LocalDateTime rentDate;
    @Column(name = "rent_period", nullable = false)
    private Integer rentPeriod;
    @Column(name = "purchase", nullable = false)
    private boolean purcashe;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false,
            foreignKey = @ForeignKey(name = "FKEY_ORDER_USER"))
    private User users;
    @ManyToOne
    @JoinColumn(name = "film_id", nullable = false,
            foreignKey = @ForeignKey(name = "FKEY_ORDER_FILM"))
    private Film film;
}
