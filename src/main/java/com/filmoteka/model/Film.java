package com.filmoteka.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
//@ToString
@Entity
@Table(name = "films")
@SequenceGenerator(name="default_generator", sequenceName = "films_seq", allocationSize = 1)
public class Film extends RootModel{

    @ManyToMany
    @JoinTable(name = "film_directors",
            joinColumns = @JoinColumn(name = "film_id"),
            foreignKey = @ForeignKey(name = "FKEY_FILMS_DIRECTORS"),
            inverseJoinColumns = @JoinColumn(name = "director_id"),
            inverseForeignKey = @ForeignKey(name = "FKEY_DIRECTORS_FILMS"))
    private List<Director> directorsList;

    @Column(name = "title", nullable = false)
    private String filmTitle;

    @Column(name = "premier_year", nullable = false)
    private LocalDate filmPremierYear;

    @Column(name = "country", nullable = false)
    private String filmCountry;

    @Column(name = "genre", nullable = false)
    @Enumerated
    private Genre filmGenre;
}
