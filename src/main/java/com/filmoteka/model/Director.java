package com.filmoteka.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
//@ToString
    @Entity
    @Table(name = "directors")
    @SequenceGenerator(name = "default_generator", sequenceName = "directors_seq", allocationSize = 1)
    public class Director extends RootModel{

//        @JsonIgnore
        @ManyToMany
        @JoinTable(name = "film_directors",
                joinColumns = @JoinColumn(name = "director_id"),
                foreignKey = @ForeignKey(name = "FKEY_DIRECTOR_FILMS"),
                inverseJoinColumns = @JoinColumn(name = "film_id"),
                inverseForeignKey = @ForeignKey(name = "FKEY_FILMS_DIRECTOR"))
        private List<Film> filmsList;

        @Column(name = "director_fio", nullable = false)
        private String directorFIO;

        @Column(name = "position", nullable = false)
        private String position;
    }
