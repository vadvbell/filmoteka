package com.filmoteka.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI projectFilms(){
        return new OpenAPI()
                .info(new Info()
                        .title("ФИЛЬМОТЕКА")
                        .description("Учебная фильмотека")
                        .version("1.0")
                        .license(new License().name("Apach 2.0"))
                        .contact(new Contact()
                                .name("111111111111111111")
                                .email("22222222222222222222")
                                .url("33333333333333333333333"))
                );

    }
}
