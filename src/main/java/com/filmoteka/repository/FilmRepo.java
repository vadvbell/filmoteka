package com.filmoteka.repository;

import com.filmoteka.model.Film;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepo extends GenericRepo<Film> {
}
