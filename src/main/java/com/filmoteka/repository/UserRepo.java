package com.filmoteka.repository;

import com.filmoteka.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends GenericRepo<User> {
}
