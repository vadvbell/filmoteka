package com.filmoteka.repository;


import com.filmoteka.model.Director;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepo extends GenericRepo<Director> {

}
