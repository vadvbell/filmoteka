package com.filmoteka.repository;

import com.filmoteka.model.RootModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface GenericRepo<T extends RootModel> extends JpaRepository<T, Long> {
}
