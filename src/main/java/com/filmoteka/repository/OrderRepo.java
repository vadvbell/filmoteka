package com.filmoteka.repository;

import com.filmoteka.model.Order;

public interface OrderRepo extends GenericRepo<Order> {
}
