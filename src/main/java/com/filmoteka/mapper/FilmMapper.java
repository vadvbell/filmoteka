package com.filmoteka.mapper;

import com.filmoteka.repository.DirectorRepo;
import com.filmoteka.dto.FilmDTO;
import com.filmoteka.model.Film;
import com.filmoteka.model.RootModel;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class FilmMapper
        extends GenericMapper<Film, FilmDTO> {
    private final DirectorRepo directorRepository;

    protected FilmMapper(ModelMapper mapper, DirectorRepo directorRepository) {
        super(Film.class, FilmDTO.class, mapper);
        this.directorRepository = directorRepository;
    }

    @PostConstruct
    @Override
    public void setupMapper() {
        modelMapper.createTypeMap(Film.class, FilmDTO.class)
                .addMappings(m -> m.skip(FilmDTO::setDirectorIDList)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmDTO.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectorsList)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmDTO source, Film destination) {
        if (!Objects.isNull(source.getDirectorIDList())) {
            destination.setDirectorsList(directorRepository.findAllById(source.getDirectorIDList()));
        }
        else {
            destination.setDirectorsList(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(Film source, FilmDTO destination) {
        destination.setDirectorIDList(getIds(source));
    }

    @Override
    protected List<Long> getIds(Film films) {
        return Objects.isNull(films) || Objects.isNull(films.getDirectorsList())
                ? null
                : films.getDirectorsList().stream()
                .map(RootModel::getId)
                .collect(Collectors.toList());
    }
}

