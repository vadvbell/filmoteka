package com.filmoteka.mapper;

import com.filmoteka.repository.OrderRepo;
import com.filmoteka.dto.UserDTO;
import com.filmoteka.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserMapper extends GenericMapper<User, UserDTO> {

    private final OrderRepo orderRepo;
    protected UserMapper(ModelMapper modelMapper,
                         OrderRepo orderRepo) {
        super(User.class, UserDTO.class, modelMapper);
        this.orderRepo = orderRepo;
    }

    @Override
    public void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setUserFilmID)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(UserDTO.class, User.class)
                .addMappings(m -> m.skip(User::setFilmsRentList)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {

    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {

    }

    @Override
    protected List<Long> getIds(User source) {
        return null;
    }
}
