package com.filmoteka.mapper;

import com.filmoteka.dto.GenericDTO;
import com.filmoteka.model.RootModel;
import jakarta.annotation.PostConstruct;
import javassist.NotFoundException;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
public abstract class GenericMapper<MOD extends RootModel, DTO extends GenericDTO>
        implements Mapper<MOD, DTO> {
    private final Class<MOD> entityClass;
    private final Class<DTO> dtoClass;
    protected final ModelMapper modelMapper;

    protected GenericMapper(Class<MOD> entityClass, Class<DTO> dtoClass, ModelMapper modelMapper) {
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
        this.modelMapper = modelMapper;
    }

    @Override
    public MOD toEntity(DTO dto) {
        return Objects.isNull(dto) ? null : modelMapper.map(dto, entityClass);
    }

    @Override
    public DTO toDTO(MOD entity) {
        return Objects.isNull(entity) ? null : modelMapper.map(entity, dtoClass);
    }

    @Override
    public List<MOD> toEntityList(List<DTO> listDTO) {
        return listDTO.stream().map(this::toEntity).toList();
    }

    @Override
    public List<DTO> toDTOList(List<MOD> listEntity) {
        return listEntity.stream().map(this::toDTO).toList();
    }

    protected Converter<DTO, MOD> toEntityConverter(){
        return context -> {
            DTO source = context.getSource();
            MOD destination = context.getDestination();
            try {
                mapSpecificFields(source, destination);
            } catch (NotFoundException e) {
                throw new RuntimeException(e);
            }
            return context.getDestination();
        };
    }
    @PostConstruct
    public abstract void setupMapper();
    protected Converter<MOD, DTO> toDTOConverter(){
        return context -> {
            MOD source = context.getSource();
            DTO destination = context.getDestination();
            mapSpecificFields(source, destination);
            return context.getDestination();
        };
    }
    protected abstract void mapSpecificFields(DTO source, MOD destination) throws NotFoundException;
    protected abstract void mapSpecificFields(MOD source, DTO destination);

    protected abstract List<Long> getIds(MOD source);
}
