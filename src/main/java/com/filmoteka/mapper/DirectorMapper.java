package com.filmoteka.mapper;

import com.filmoteka.repository.FilmRepo;
import com.filmoteka.dto.DirectorDTO;
import com.filmoteka.model.Director;
import com.filmoteka.model.RootModel;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class DirectorMapper extends GenericMapper<Director, DirectorDTO> {
    private final FilmRepo filmRepo;

    protected DirectorMapper(ModelMapper modelMapper,
                             FilmRepo filmRepo) {
        super(Director.class, DirectorDTO.class, modelMapper);
        this.filmRepo = filmRepo;
    }

    @PostConstruct
    public void setupMapper() {
        modelMapper.createTypeMap(Director.class, DirectorDTO.class)
                .addMappings(m -> m.skip(DirectorDTO::setFilmIDList))
                .setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(DirectorDTO.class, Director.class)
                .addMappings(m -> m.skip(Director::setFilmsList))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(DirectorDTO source, Director destination) {
        if (!Objects.isNull(source.getFilmIDList())) {
            destination.setFilmsList(filmRepo.findAllById(source.getFilmIDList()));
        } else {
            destination.setFilmsList(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(Director source, DirectorDTO destination) {
        destination.setFilmIDList(getIds(source));
    }

    @Override
    protected List<Long> getIds(Director source) {
        return Objects.isNull(source) || Objects.isNull(source.getFilmsList())
                ? Collections.emptyList() :
                source.getFilmsList().stream()
                        .map(RootModel::getId)
                        .collect(Collectors.toList());
    }
}
