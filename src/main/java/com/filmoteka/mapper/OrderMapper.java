package com.filmoteka.mapper;

import com.filmoteka.repository.FilmRepo;
import com.filmoteka.repository.UserRepo;
import com.filmoteka.dto.OrderDTO;
import com.filmoteka.model.Order;
import jakarta.annotation.PostConstruct;
import javassist.NotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderMapper
        extends GenericMapper<Order, OrderDTO> {
    private final FilmRepo filmRepo;
    private final UserRepo userRepo;

    protected OrderMapper(ModelMapper mapper,
                          FilmRepo filmRepo,
                          UserRepo userRepo) {
        super(Order.class, OrderDTO.class, mapper);
        this.filmRepo = filmRepo;
        this.userRepo = userRepo;
    }

    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Order.class, OrderDTO.class)
                .addMappings(m -> m.skip(OrderDTO::setUserID))
                .addMappings(m -> m.skip(OrderDTO::setFilmID))
                .setPostConverter(toDTOConverter());

        super.modelMapper.createTypeMap(OrderDTO.class, Order.class)
                .addMappings(m -> m.skip(Order::setUsers))
                .addMappings(m -> m.skip(Order::setFilm))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OrderDTO source, Order destination) throws NotFoundException {
        destination.setFilm(filmRepo.findById(source.getFilmID()).orElseThrow(() -> new NotFoundException("Фильма не найдено")));
        destination.setUsers(userRepo.findById(source.getUserID()).orElseThrow(() -> new NotFoundException("Пользователя не найдено")));
    }

    @Override
    protected void mapSpecificFields(Order source, OrderDTO destination) {
        destination.setUserID(source.getUsers().getId());
        destination.setFilmID(source.getFilm().getId());
    }

    @Override
    protected List<Long> getIds(Order entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }
}

