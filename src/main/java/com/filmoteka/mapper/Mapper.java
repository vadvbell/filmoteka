package com.filmoteka.mapper;

import com.filmoteka.dto.GenericDTO;
import com.filmoteka.model.RootModel;

import java.util.List;

public interface Mapper<MOD extends RootModel, DTO extends GenericDTO> {
    MOD toEntity(DTO dto);
    DTO toDTO(MOD entity);
    List<MOD> toEntityList(List<DTO> listDTO);
    List<DTO> toDTOList(List<MOD> listEntity);
}
